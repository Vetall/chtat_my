<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class IfUserAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            $user = User::query()
                ->whereId(Auth::user()->id)
                ->first();
            if($user->isAdmin == 0){
                return redirect('/');
            }
        }else{
          return  redirect('/');
        }

        return $next($request);
    }
}
