<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Block;
use App\Models\Contracts\BlockContract;
use Illuminate\Http\Request;

class BlockController extends Controller{

    public function get(Request $request)
    {
        $search = $request['search'];

        if($request['limit'] == null){
            $request['limit'] = 4;
        }

        $block = Block::query()
            ->limit($request['limit'])
            ->skip($request['skip'])
            ->where(BlockContract::NAME, 'like', '%' . $search . '%')
            ->orWhere(BlockContract::EMAIL, 'like', '%' . $search . '%')
            ->get();

        return view('admin.block.list')->with(['block' => $block]);
    }

    public function send(Request $request, $id)
    {
        $block = Block::query()->find($id);
        if($block->ban == 1) {
            $block->ban = 0;
        }else{
            $block->ban = 1;
        }
        $block->save();

        return redirect('/admin/block');
    }
}