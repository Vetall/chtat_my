<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10.08.17
 * Time: 22:20
 */

namespace App\Http\Controllers;

use App\Models\Chatroom;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainPageController extends Controller
{

    public function index(Request $request)
    {
        if(Auth::user()) {
            $users = User::query()
                ->where('id', '<>', Auth::user()->id)
                ->where('isAdmin', 0)
                ->where('ban', 0)
                ->get();
            return view('pages.index')->with(['users' => $users]);
        }else{
            return view('pages.index');
        }
    }

    public function getUsers(Request $request)
    {
        if(Auth::user()) {
            $users = User::query()
                ->where('id', '<>', Auth::user()->id)
                ->where('isAdmin', 0)
                ->where('ban', 0)
                ->get();
//            dd($users[0]->isOnline());
            foreach ($users as $user){
//                $user->put('online', $user->isOnline);
                $user['isOnline'] = $user->isOnline();
            }
            return response()->json($users);
        }else{
            return view('pages.index');
        }
    }

    public function chatroom(Request $request, $id)
    {
        $host = $request->getHttpHost();

        $chatRoom = Chatroom::query()
            ->where('first_user', $id)
            ->where(function (\Illuminate\Database\Eloquent\Builder $builder) use($id){
               return $builder->where('first_user', $id)
                    ->where('second_user', Auth::user()->id);
            })
            ->orWhere(function (\Illuminate\Database\Eloquent\Builder $builder) use($id){
                return $builder->where('second_user', $id)
                    ->where('first_user', Auth::user()->id);
            })
            ->first();
        if(Auth::user()->id == $id){
            return view('pages.errorchat');
        }

        if(!$chatRoom){
           $chatRoom = Chatroom::query()->create([
                'first_user' => Auth::user()->id,
                'second_user' => $id
            ]);
        }

        $messages = Chatroom::query()
            ->with(['message.user'])
            ->where(function (\Illuminate\Database\Eloquent\Builder $builder) use($id){
                return $builder->where('first_user', $id)
                    ->where('second_user', Auth::user()->id);
            })
            ->orWhere(function (\Illuminate\Database\Eloquent\Builder $builder) use($id){
                return $builder->where('second_user', $id)
                    ->where('first_user', Auth::user()->id);
            })

            ->first();
//        dd($messages->toArray());
        if(!($messages->first_user == Auth::user()->id)){
            $user = $messages->first_user;
        }else{
            $user = $messages->second_user;
        }
//        dd($messages['message']->toArray());

        return view('pages.chatroom')->with(['messages' => $messages['message'], 'chatroom_id' => $messages->id, 'user_id' => $user, 'host' => $host . '/']);

    }

    public function send(Request $request, $id)
    {
        Message::query()->create([
            'content' => $request['content'],
            'chatroom_id' => $id,
            'user_id' => Auth::user()->id
        ]);

       return redirect('/chatroom/'.$request['user_id']);
    }
}