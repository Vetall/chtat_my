<?php

namespace App\Models;

use App\Models\Contracts\BlockContract;
use Illuminate\Database\Eloquent\Model;

class Block extends Model{

    protected $table = BlockContract::_TABLE;

    protected $fillable = [
        BlockContract::NAME,
        BlockContract::LAST_NAME,
        BlockContract::EMAIL,
    ];

}