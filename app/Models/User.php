<?php

namespace App\Models;

use App\Models\Contracts\UserContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        UserContract::NAME,
        UserContract::LAST_NAME,
        UserContract::EMAIL,
        UserContract::PASSWORD,
        UserContract::AVATAR_PATH,
        UserContract::AVATAR_LOGO
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        UserContract::PASSWORD,
        UserContract::REMEMBER_TOKEN
    ];

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
}
