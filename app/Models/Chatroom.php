<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Chatroom extends Model{

    protected $table ='chatrooms';

    protected $fillable = [
        'first_user',
        'second_user'
    ];


    public function message()
    {
        return $this->hasMany(Message::class);
    }


}