<?php
/**
 * Created by PhpStorm.
 * User: oleh
 * Date: 10.08.17
 * Time: 11:29
 */

namespace App\Models\Contracts;

class UserContract{
    const _TABLE = 'users';
    const NAME = 'name';
    const LAST_NAME = 'last_name';
    const EMAIL = 'email';
    const AVATAR_PATH = 'avatar_path';
    const AVATAR_LOGO = 'avatar_logo';
    const PASSWORD = 'password';
    const REMEMBER_TOKEN = 'remember_token';
}