<?php

namespace App\Events;

use App\Models\CallBack;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class AdminCallBackMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $callBack;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CallBack $callBack)
    {
        $this->callBack = $callBack;
    }
}
