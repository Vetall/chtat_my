<?php

namespace App\Listeners;

use App\Events\AdminCallBackMail;
use App\Mail\SendAdminNotificationFeedBack;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendMailToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminCallBackMail  $event
     * @return void
     */
    public function handle(AdminCallBackMail $event)
    {
        Mail::to(config('admin.email'))->send(new SendAdminNotificationFeedBack($event->callBack));
    }
}
