<?php

namespace App\Mail;

use App\Models\CallBack;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminNotificationFeedBack extends Mailable
{
    use Queueable, SerializesModels;

    public $callBack;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CallBack $callBack)
    {
        $this->callBack = $callBack;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from($this->callBack->email)
            ->view('mail.send')
            ->with(['callBack' => $this->callBack]);
    }
}
