<?php

namespace App\Mail;

use App\Models\CallBack;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResponseFromAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $callBack;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CallBack $callBack, $message)
    {

        $this->callBack = $callBack;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(config('admin.email'))
            ->view('mail.admin_send')
            ->with(['callBack' => $this->callBack, 'messageAdmin' => $this->message]);
    }
}
