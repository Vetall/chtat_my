@extends('main')

@section('stylesheets')

@endsection

@section('content')
    @if(\Illuminate\Support\Facades\Auth::user())
        <section class="main">
            <ul class="list-group">
                <a href="/">Back to your room</a>
                @foreach($messages as $msg)
                    <li class="list-group-item message-item"><img style="width: 40px; height: 40px" src="{{'http://'.$host}}{{$msg['user']['avatar_path']}}"/>
                        <span style="color: green">{{$msg['user']['name']}}:</span></br>
                        <h4>{{$msg->content}}</h4>
                        <span style="float: right; position: relative;bottom: 13px">{{$msg->created_at}}</span>
                    </li>
                @endforeach
            </ul>
            <div class="row message-send-box">
                <form method="post" action="/message/send/{{$chatroom_id}}">
                    <div class="col-xs-12 col-md-11">
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <textarea class="form-control" rows="3" name="content"></textarea>
                            <input type="hidden" name="user_id" value="{{$user_id}}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-1 text-center">
                        <input class="btn  btn-lg send-btn" type="submit">
                    </div>
                </form>
            </div>
        </section>
    @endif
@endsection

{{--@section('scripts')--}}

{{--@endsection--}}