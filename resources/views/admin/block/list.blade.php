@extends('backpack::layout')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <!-- Default box -->

            <div class="box">
                <div class="box-header with-border">

                    <div id="datatable_button_stack"><h2>List Users</h2></div>
                </div>

                <div class="box-body table-responsive">
                    <div id="crudTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">

                            <div class="col-sm-12">
                                <div id="crudTable_filter" class="dataTables_filter text-right">
                                    <form action="/admin/block" method="get">
                                        <label>Search: <input type="search" name ="search" class="form-control input-sm" placeholder="" aria-controls="crudTable">
                                            <input type="submit" class="btn btn-primary" value="Search">
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <table id="crudTable" class="table table-bordered table-striped display dataTable" role="grid" aria-describedby="crudTable_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Id: activate to sort column ascending" style="width: 102px;">id</th>
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 148px;">name</th>
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Message: activate to sort column ascending" style="width: 191px;">email</th>
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 126px;">ban</th>
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 172px;">date</th>
                                        <th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" aria-label="Options: activate to sort column ascending" style="width: 191px;">options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($block as $b)
                                        <tr data-entry-id="1" role="row" class="odd">

                                            <td>{{$b->id}}</td>
                                            <td>{{$b->name}}</td>
                                            <td>{{$b->email}}</td>
                                            @if($b->ban == 0)
                                                <td>Не забанений</td>
                                            @else
                                                <td>Забанений</td>
                                            @endif
                                            <td>{{$b->created_at}}</td>

                                            <td>
                                                <form action="/admin/block/send/{{$b->id}}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" name="send" class="btn btn-primary" value="Block user">
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th rowspan="1" colspan="1">id</th>
                                        <th rowspan="1" colspan="1">name</th>
                                        <th rowspan="1" colspan="1">email</th>
                                        <th rowspan="1" colspan="1">ban</th>
                                        <th rowspan="1" colspan="1">date</th>
                                        <th rowspan="1" colspan="1">options</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                </div><!-- /.box-body -->


            </div>

        </div>
    </div>
@endsection

