<nav class="navbar navbar-default header">
    <div class="header-body">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img src="{{asset('img/apache-logo.png')}}" />
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right custom-menu-btn">

                @if(Auth::guest())
                <li class="last-link sing-link">
                    <a href="#">Sign In
                        {{--<i class="fa fa-align-center fa-2x" aria-hidden="true"></i>--}}
                    </a>
                </li>
                @else
                    <li class="last-link"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out fa-2x" aria-hidden="true"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                @endif
                {{--<li id="search_btn" class="last-link border-link"><a href="#"> <i class="fa fa-search fa-2x" aria-hidden="true"></i> </a></li>--}}
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>