<div class="registration-content reg-none">
    <div class="panel-group" id="accordion-reg" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingAuth">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-reg" href="#collapseAuth"
                       aria-expanded="true" aria-controls="collapseAuth">
                        log in
                    </a>
                </h4>
            </div>
            <div id="collapseAuth" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAuth">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <ul id="error"></ul>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input style="display: none" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>{{--Remember Me--}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary"  id="btn">
                                                Login
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            {{----}}
                            {{----}}
                            {{----}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingReg">
                <h4 class="panel-title act">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-reg"
                       href="#collapseReg" aria-expanded="false" aria-controls="collapseReg">
                        registration
                    </a>
                </h4>
            </div>
            <div id="collapseReg" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingReg">
                <div class="panel-body">
                    <form class="form-horizontal" id="signup-form" method="POST" enctype="multipart/form-data" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <label for="name" class="col-md-4 control-label">Last Name</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"  autofocus>

                            {{--@if ($errors->has('name'))--}}
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            {{--@endif--}}
                        </div>

                        <label for="name" class="col-md-4 control-label">Photo</label>

                        <div class="col-md-6">

                            {{--<input name="image" type="file" class="hide" />--}}
                            {!! Form::file('image', array('class' => 'image')) !!}
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password1" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password1" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password_confirm" class="col-md-4 control">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password_confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('/') }}/AjaxLogin/AjaxLogin.js"></script>
<script>
    AL = new Login({
        email:"email",
        password:"password",
        btn:"btn",
        url:"/login",
        successUrl:"/",
        mode:"toast" ///alert or toast
    });
</script>