<!DOCTYPE html>
<html>
<head>
    @include('partials.head')
    @yield('stylesheets')
</head>
<body>
@include('partials.nav')
<!-- sing/registration -->
@include('partials.login')


@yield('content')


@include('partials.footer')
@yield('scripts')
</body>
</html>