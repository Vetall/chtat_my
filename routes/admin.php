<?php
/**
 * Created by PhpStorm.
 * User: oleh
 * Date: 11.08.17
 * Time: 13:49
 */
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['isadmin'],
    'namespace' => 'Admin'
], function() {
    Route::get('/block', 'BlockController@get');
    Route::post('/block/send/{id}', 'BlockController@send');
});

