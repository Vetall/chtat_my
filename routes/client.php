<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10.08.17
 * Time: 22:18
 */
Route::middleware(['user.ban'])->group(function () {
    Route::get('/', 'MainPageController@index');
    Route::get('/users', 'MainPageController@getUsers');
    Route::get('/chatroom/{id}', 'MainPageController@chatroom');
    Route::post('/message/send/{chatroom_id}', 'MainPageController@send');
});