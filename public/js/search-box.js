$('#search_btn').click(function(){
    $('#search-box').toggleClass('hidden');
    $('#search_btn').toggleClass('search_open');

    if($('#search_btn a i.fa').hasClass('fa-search')){
        $('#search_btn a i.fa').removeClass('fa-search').addClass('fa-times');
    }else {
        $('#search_btn a i.fa').removeClass('fa-times').addClass('fa-search');
    }
});
