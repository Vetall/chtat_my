$(function(){
  

  
  
  $('.search-box__link').click(function(){
    $('.rem').removeClass('display');
    $('.body').removeClass('body-overlaid');
  });
  
  // menu show hide list
  $('.hide-sub-menu').parent().parent().each(function(){
        $(this).hover(function(){
        $(this).find('.block-nav').removeClass('hide-show-sub');
      },function(){
        $(this).find('.block-nav').addClass('hide-show-sub');
      });
  });
  
  // accordion registration sign
  $('.registration-content #headingAuth a').click(function(){
        $(this).parent().toggleClass('act');  
        $('#headingReg .panel-title').removeClass('act')
  });
  $('.registration-content #headingReg a').click(function(){
        $(this).parent().toggleClass('act');  
        $('#headingAuth .panel-title').removeClass('act')
  });
  
  //sign block
  $('.sign').click(function(){
    $('.component.search-box').removeClass('search-box--visible');
    $('.rem').toggleClass('display');
    if( $('.rem').hasClass('display') ){
      $('.body #header').addClass('scrolled-fixed');
    }
    else{
      $('.body #header').removeClass('scrolled-fixed');
    }
    $('body').toggleClass('body-overlaid');
    $('body').removeClass('body--overlaid');
  });
  
  //vacancy accordion
  
  $('.vacan-acordion .panel-title a').on('click',function(){
    if($(this).parents('.panel-heading').hasClass('active')){
      $(this).parents('.panel-heading').removeClass('active');
    }
    else{
      $('.vacan-acordion .panel-heading').removeClass('active');
      $(this).parents('.panel-heading').addClass('active');
    }
  });
  
  
  $('.vacancy-content .panel-title.colOn a').click(function(){
    $('.colTw,.colThr').removeClass('acive-vacancy');
    $(this).parent().toggleClass('acive-vacancy');
  });
  $('.vacancy-content .panel-title.colTw a').click(function(){
    $('.colOn,.colThr').removeClass('acive-vacancy');
    $(this).parent().toggleClass('acive-vacancy');
  });
  $('.vacancy-content .panel-title.colThr a').click(function(){
    $('.colOn,.colTw').removeClass('acive-vacancy');
    $(this).parent().toggleClass('acive-vacancy');
  });


    $('.rotate-icon').click(function() {
        $(this).find("img").toggleClass('rotated');
    });


  //add body shadow and content
  $('.header .language').click(function(){
      $('.language-content').toggleClass('lang-block');
      $('.registration-content').addClass('reg-none');
      $('body').removeClass('body-shadow-sign');
      $('body').toggleClass('body-shadow-lang');
  });

  $('.sing-link').click(function(){
      $(this).toggleClass('act-sign');
      $('.language-content').removeClass('lang-block');
      $('.registration-content').toggleClass('reg-none');
      $('body').removeClass('body-shadow-lang');
      $('body').toggleClass('body-shadow-sign');
  });
  $('.down-menu ul li.down-menu-link:not(li.down-menu-link.home-link)').each(function(){
    $(this).hover(function(){
      $('body').addClass('body-down-menu');
    },function(){
      $('body').removeClass('body-down-menu');
    });
  });
  
  //carousel stop
  $('.carousel').carousel({
    pause: true,
    interval: false
  });
  
  //back to top
  $(window).scroll(function(){
    back();
  });
  function back(){
    if( $(window).scrollTop() > 100 ){
      $('.back-top').addClass('back-top-visible');
    }
    else{
      $('.back-top').removeClass('back-top-visible');
    }
  }
  back();
  $('.back-top div').click(function(){
    $('html,body').animate({
      scrollTop: 0
    },800);
  });
  
  $('.navbar-toggle').click(function(){
    $('#bs-example-navbar-collapse-1').hide();
    $('.min-menu').toggleClass('show-right');
    $(this).toggleClass('toggle-min-menu');
  });
  
  // language link show-hide
  $('.language-content li a').click(function(){
    $('.language-content li ul').hide();
    $(this).next().show();
  })
  
});

  // });